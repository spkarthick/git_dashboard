var express = require('express');
var app = express();
var GitHubApi = require("github");

var github = new GitHubApi({
	// optional
	debug: true,
	protocol: "https",
	host: "api.github.com", // should be api.github.com for GitHub
	//pathPrefix: "/api/v3", // for some GHEs; none for GitHub
	headers: {
		"user-agent": "sp" // GitHub is happy with a unique user agent
	},
	Promise: require('bluebird'),
	followRedirects: false, // default: true; there's currently an issue with non-get redirects, so allow ability to disable follow-redirects
	timeout: 50000
});

github.authenticate({
    type: "basic",
    username: "******",
    password: "******"
});

app.get('/pullrequests', function(req, res){
	github.pullRequests.getAll({ 
		'owner': 'jquery',
		'repo': 'jquery' 
	}).then(function(response) {
		res.send(response.data);
	});
});

app.get('/pullrequests/comments/:id', function(req, res){
	github.issues.getComments({ 
		'owner': 'jquery',
		'repo': 'jquery',
		'number': req.params.id
	}).then(function(response) {
		res.send(response.data);
	});
});

app.listen(3000);